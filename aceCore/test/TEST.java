import org.junit.Test;

import com.ace.external.model.CheckUser;
import com.ace.utils.GsonUtil;


public class TEST {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String returnxml = "<CheckUser> <xs:schema id=\"CheckUser\" xmlns=\"\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:msdata=\"urn:schemas-microsoft-com:xml-msdata\"> <xs:element name=\"CheckUser\" msdata:IsDataSet=\"true\" msdata:UseCurrentLocale=\"true\"> <xs:complexType> <xs:choice minOccurs=\"0\" maxOccurs=\"unbounded\"> <xs:element name=\"User\"> <xs:complexType> <xs:sequence> <xs:element name=\"UserID\" type=\"xs:string\" minOccurs=\"0\" /> <xs:element name=\"TokenID\" type=\"xs:string\" minOccurs=\"0\" /> <xs:element name=\"Status\" type=\"xs:string\" minOccurs=\"0\" /> </xs:sequence> </xs:complexType> </xs:element> <xs:element name=\"SysErr\"> <xs:complexType> <xs:sequence> <xs:element name=\"ErrMsg\" type=\"xs:string\" minOccurs=\"0\" /> </xs:sequence> </xs:complexType> </xs:element> </xs:choice> </xs:complexType> </xs:element> </xs:schema> <User> <UserID>inglife.com.hk\27221374t</UserID> <TokenID /> <Status>Failed!</Status> </User> <SysErr> <ErrMsg /> </SysErr> </CheckUser>";
		GsonUtil.fromXml(returnxml, CheckUser.class);
	}
	
	@Test
	public void TestAAA(){
		String returnxml = "<CheckUser><User><UserID>27221374t</UserID><TokenID /><Status>Failed!</Status></User></CheckUser>";
		//String returnxml = "<CheckUser> <xs:schema id=\"CheckUser\" xmlns=\"\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:msdata=\"urn:schemas-microsoft-com:xml-msdata\"> <xs:element name=\"CheckUser\" msdata:IsDataSet=\"true\" msdata:UseCurrentLocale=\"true\"> <xs:complexType> <xs:choice minOccurs=\"0\" maxOccurs=\"unbounded\"> <xs:element name=\"User\"> <xs:complexType> <xs:sequence> <xs:element name=\"UserID\" type=\"xs:string\" minOccurs=\"0\" /> <xs:element name=\"TokenID\" type=\"xs:string\" minOccurs=\"0\" /> <xs:element name=\"Status\" type=\"xs:string\" minOccurs=\"0\" /> </xs:sequence> </xs:complexType> </xs:element> <xs:element name=\"SysErr\"> <xs:complexType> <xs:sequence> <xs:element name=\"ErrMsg\" type=\"xs:string\" minOccurs=\"0\" /> </xs:sequence> </xs:complexType> </xs:element> </xs:choice> </xs:complexType> </xs:element> </xs:schema> <User> <UserID>inglife.com.hk\27221374t</UserID> <TokenID /> <Status>Failed!</Status> </User> <SysErr> <ErrMsg /> </SysErr> </CheckUser>";
		//String returnxml = "<User><userID>inglife.com.hk\27221374t</userID><tokenID /><status>Failed!</status></User>";
		CheckUser checkUser = GsonUtil.fromXml(returnxml, CheckUser.class);
		if(checkUser == null)
			System.out.println("checkUser is null");
		else
			System.out.println("checkUser:"+checkUser.toString());
	}

}
