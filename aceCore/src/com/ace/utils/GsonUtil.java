package com.ace.utils;

import java.lang.reflect.Type;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.stanfy.gsonxml.GsonXml;
import com.stanfy.gsonxml.GsonXmlBuilder;
import com.stanfy.gsonxml.XmlParserCreator;

public class GsonUtil {
	
	private final static Logger logger = LoggerFactory.getLogger(GsonUtil.class);
	
	/** Parser factory. */
	protected static final XmlParserCreator PARSER_FACTORY = new XmlParserCreator() {
	    @Override
	    public XmlPullParser createParser() {
	      try {
	        return XmlPullParserFactory.newInstance().newPullParser();
	      } catch (final XmlPullParserException e) {
	    	  logger.error(e.toString(), e);
	        throw new RuntimeException(e);
	      }
	    }
	};
	
	public static <T> T fromXml(String xml, Class<T> cls){
		GsonXml gsonXml = new GsonXmlBuilder()
	     .setXmlParserCreator(PARSER_FACTORY)
	     .setSameNameLists(true)
	     .create();
		try {
			return  gsonXml.fromXml(xml, cls);
		}  catch (Exception e) {
			logger.error(e.toString(), e);
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static <T> T fromJson(String json, Class<T> cls, boolean returnNullIfError){
		Gson gson = new Gson();
		try {
			return gson.fromJson(json, cls);
		}catch (JsonSyntaxException e){
			logger.error("json : " + json);
			logger.error(e.toString(), e);
		}
		try {
			if (returnNullIfError){
				return null;
			}else{
				return cls.newInstance();
			}
		} catch (InstantiationException e) {
			logger.error(e.toString(), e);
		} catch (IllegalAccessException e) {
			logger.error(e.toString(), e);
		}
		return null;
	}
	
	public static <T> T fromJson(String json, Class<T> cls){
		return fromJson(json, cls, false);
	}
	
	public static <T> T fromJson(String json, Type typeOfT){
	    Gson gson = new Gson();
	    return gson.fromJson(json, typeOfT);
	}
	
	public static String toJson(Object obj){
		Gson gson = new Gson();
		return gson.toJson(obj);
	}
}
