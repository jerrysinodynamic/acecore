package com.ace.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.ace.object.Agent;
import com.ace.rowmapper.AgentMapper;

@Component
public class AgentDao {
	
	private final static Logger logger = LoggerFactory.getLogger(AgentDao.class);
	
	public Agent getPersonDetail(Connection conn) {

		String sql = "select * from agent_profile";
		Agent agent;
		
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			agent = new Agent();
			while (rs.next()) {
				agent.setAgentCode(rs.getString("agent_code"));
			}
 
		} catch (SQLException e) {
			throw new RuntimeException(e);
 
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {}
			}
		}
		
		return agent;
		
	}
	
	public List<Agent> getPersonDetailWithMapper(DataSource dataSource) {

		String sql = "SELECT * FROM agent_profile";
		List<Agent> agentList = new ArrayList<>();
		
		try{
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			agentList = (List<Agent>)jdbcTemplate.query(sql, new AgentMapper());
		}catch(DataAccessException e){
			logger.error(e.getMessage(), e);
		}
		
		return agentList;
	}

}
