package com.ace.external.model;

import com.google.gson.annotations.SerializedName;

public class User {
	
	@SerializedName("UserID")
	private String UserID;
	@SerializedName("TokenID")
	private String TokenID;
	@SerializedName("Status")
	private String Status;
	
	@Override
	public String toString() {
		return "User [UserID=" + UserID + ", TokenID=" + TokenID + ", Status="
				+ Status + "]";
	}

}

