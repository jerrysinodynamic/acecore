package com.ace.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.ace.object.Agent;

public class AgentMapper implements RowMapper<Agent> {

	public Agent mapRow(ResultSet rs, int rowNum) throws SQLException {
		Agent agent = new Agent();
		agent.setAgentCode(rs.getString("agent_code"));
		return agent;
	}
}